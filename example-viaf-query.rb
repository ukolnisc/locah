require 'rubygems'
require 'sru'
require 'nokogiri'

client = SRU::Client.new("http://viaf.org/viaf/search", :parser=>"libxml")
opts = {:maximumRecords=>5, :startRecord=>1, :recordSchema=>"VIAF"}
results = client.search_retrieve("local.personalNames=Ernest Shackleton", opts)

results.each do |result|
	doc = Nokogiri::XML(result.to_s)
	doc.xpath('//ns2:mainHeadings/ns2:data/ns2:text', 'ns2' => "http://viaf.org/viaf/terms#").each do |node|
	  puts node
	end
	doc.xpath('//ns2:viafID', 'ns2' => "http://viaf.org/viaf/terms#").each do |node|
	  puts node
	end
end