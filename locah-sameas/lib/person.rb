require 'rubygems'
require 'json'
require File.expand_path("../../../locah-web/lib/rosso.rb", __FILE__)

class Person
	PERSON = "http://data.archiveshub.ac.uk/id/person/".freeze
	
	attr_reader :keys
	
	def initialize
		@rosso = Rosso.new({:redis_key => "viaf"})
		@keys = @rosso.keys("#{PERSON}*")
	end
	
	def count
		@keys.size
	end
	
	def lookup(key)
		puts "#{PERSON}#{key}"
		@p = @rosso.lrange("#{PERSON}#{key}")
		self
	end
	
	def name
		JSON.parse(@p.first)['name']
	end
	
	def viaf_id
		JSON.parse(@p.first)['viaf_id']
	end
	
end