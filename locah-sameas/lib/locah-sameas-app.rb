require 'rubygems'
require 'sinatra/base'
require 'sinatra_more/markup_plugin'
require 'sinatra_more/routing_plugin'
require 'sinatra/content_for'

require 'net/http'
require 'uri'

require 'person'

class LocahSameasApp < Sinatra::Base
	register SinatraMore::MarkupPlugin
	register SinatraMore::RoutingPlugin
	helpers Sinatra::ContentFor
	
	map(:root).to("/")
	
	helpers do
	  include Rack::Utils
	  alias_method :h, :escape_html
	end
	
	configure do |app|
    set :views, File.dirname(__FILE__) + "/../views"
    set :public, File.dirname(__FILE__) + "/../public"
  end
	
	get '/' do
		erb :index
	end
	
	get '/viaf/?*' do
		p = Person.new
		if 0 < params[:splat].size
			@key = params[:splat].to_s.sub(/person\//, '')
			@p = p.lookup(@key.sub(/\/\z/,""))
		end
		@count = p.count
		erb :viaf
	end

	helpers do
	  def partial(template, *args)
	    options = args.extract_options!
	    options.merge!(:layout => false)
	    if collection = options.delete(:collection) then
	      collection.inject([]) do |buffer, member|
	        buffer << haml(template, options.merge(
	                                  :layout => false, 
	                                  :locals => {template.to_sym => member}
	                                )
	                     )
	      end.join("\n")
	    else
	      haml(template, options)
	    end
	  end
	end
		
end