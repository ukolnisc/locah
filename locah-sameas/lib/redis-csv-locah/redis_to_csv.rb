require 'rubygems'
require 'fastercsv'
require 'json'

require 'person'

class RedisToCSV

	def initialize
	end
	
	# example action to return the contents
	# of a table in CSV format
	def export_users
		p = Person.new
		stream_csv do |csv|
			csv << ["name","archive_hub_uri","archive","viaf_uri","match?"]
			p.keys.each do |key|
				person = p.lookup(key)
				viaf = "http://viaf.org/viaf/#{[person.viaf_id]}/"
				archive = person.lookup_archive(key)
				csv << [person.name,key,archive,viaf]
			end
		end
	end

	private
		def stream_csv
			filename = "locah_data.csv"
			FasterCSV.open(filename, "a") do |csv|
				yield csv
			end
		end
end

r = RedisToCSV.new
r.export_users