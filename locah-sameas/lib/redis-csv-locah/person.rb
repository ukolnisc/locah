require 'rubygems'
require 'json'
require File.expand_path("../../../../locah-web/lib/glimmer.rb", __FILE__)
require 'rosso'

class Person
	PERSON = "http://data.archiveshub.ac.uk/id/person/".freeze
	
	attr_reader :keys
	
	def initialize
		@rosso = Rosso.new({:redis_key => "viaf"})
		@keys = @rosso.keys("#{PERSON}*")
		@glimmer = Glimmer.new
	end
	
	def count
		@keys.size
	end
	
	def lookup(key)
		@p = @rosso.lrange(key)
		self
	end
	
	def name
		JSON.parse(@p.first)['name']
	end
	
	def viaf_id
		JSON.parse(@p.first)['viaf_id']
	end
	
	def lookup_archive(key)
		sparql = <<-EOL
			SELECT ?p ?result WHERE { 
			?concept <http://xmlns.com/foaf/0.1/focus> <#{key}>.
			  ?archresource ?p ?concept.
			?s <http://xmlns.com/foaf/0.1/topic> ?archresource;
			  <http://www.w3.org/2000/01/rdf-schema#seeAlso> ?result.
			}
		EOL
		@results = @glimmer.sparql_select(sparql,false)
		result = ""
		parsed = JSON.parse(@results.content)
		parsed['results'].each do |result|
			result[1].each do |stuff|
				parsed['head']['vars'].each do |values|
					case type = values
	        	when 'p'
							# do nothing
	        	when 'result'
	          	# hashy.store("given_name",stuff[values]['value'])
							result = stuff[values]['value'] unless !stuff[values]['value'].include?("http://archiveshub.ac.uk/data/")
						else
          		raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
					end
				end
			end
		end
		result
	end
end