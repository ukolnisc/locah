require 'rubygems'
require 'yaml'
require 'redis'

class Rosso
		
	def initialize(options={})
		read_config
		@options = @defaults.merge(options)
		@db = Redis.new(@options)
		@host = @options[:host]
		@port = @options[:port]
		@redis_key = @options[:redis_key]
	end
	
	def lrange(redis_key=@redis_key,start=0, finish=-1)
	  @db.lrange(redis_key, start, finish)
	end

	# push to head
	def lpush(data,redis_key=@redis_key)
	  @db.lpush(redis_key, data)
	end

	# push to tail
	def rpush(data,redis_key=@redis_key)
	  @db.rpush(redis_key, data)
	end
	
	# count > 0: Remove elements equal to value moving from head to tail.
	# count < 0: Remove elements equal to value moving from tail to head.
	# count = 0: Remove all elements equal to value.
	def lrem(data, redis_key=@redis_key, count=0)
		@db.lrem(redis_key, data, count)
	end

	def incr(key)
	  @db.incr(key)
	end
	
	def keys(key)
		@db.keys(key)
	end

	# need to implement
	def sadd
	end

	private

	def read_config
		begin
			file = File.expand_path("../redis.yaml", __FILE__)
			config = YAML.load_file(file)
			@host = config["config"]["host"] || "localhost"
			@port = (config["config"]["port"] || 6379).to_i
			@redis_key = config["config"]["redis_key"] || "default"
		rescue Exception => e
			puts "Err: #{e}"
			@host = "localhost"
			@port = 6379
			@redis_key = "default"
		end
		@defaults = {:host => @host, :port => @port, :redis_key => @redis_key}
	end
end