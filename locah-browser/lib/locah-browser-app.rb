require 'rubygems'
require 'sinatra/base'
require 'sinatra_more/markup_plugin'
require 'sinatra_more/routing_plugin'
require 'rdf'
require 'rdf/json'
require 'domainatrix'

require 'dbpedia'
require 'person'

class LocahBrowserApp < Sinatra::Base
	register SinatraMore::MarkupPlugin
	register SinatraMore::RoutingPlugin

	map(:root).to("/")
	map(:uri).to("/uri/:query")
	map(:result).to("/results/:query")
	map(:results).to("/results")
	map(:enrich).to("/enrich")
	map(:enrich).to("/enrich/:query")
	
	helpers do
	  include Rack::Utils
	  alias_method :h, :escape_html
	  
	  def image?(file)
	    @image = ""
      if file.to_s.include?(".jpg") or file.to_s.include?(".png") or file.to_s.include?(".svg")
        "<img alt=\"\" src=\"#{file}\" />"
      else
        file
      end
    end
    
    def parse_url(url)
      parsed = Domainatrix.parse(url)
      "#{parsed.subdomain}.#{parsed.domain}.#{parsed.public_suffix}#{parsed.path.gsub(/\bid/, 'doc')}"
    end
	end
	
	configure do |app|
    set :views, File.dirname(__FILE__) + "/../views"
    set :public, File.dirname(__FILE__) + "/../public"
  end
	
	get '/' do
		erb :index
	end
	
	get '/person/*' do
    p = Person.new
    p.load_graph("http://#{params[:splat].first}.rdf")
    dbpedia_uri = p.query_graph
    @dbpedia_uri = dbpedia_uri
    @params = params[:splat].first
    if dbpedia_uri.eql?("") || nil
      @results = []
    else
      d = DBpedia.new
      d.load_graph(dbpedia_uri)
      @results = d.query_graph
    end
    erb :person
	end
	
	get '/people/' do
    @p = Person.new
    @p.sparqly
    erb :people
  end
end