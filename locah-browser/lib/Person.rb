require 'rubygems'
require 'json'
require 'uri'
require 'linkeddata'
require 'rdf'
require 'rdf/ntriples'

require File.expand_path("../../../locah-web/lib/glimmer.rb", __FILE__)

class Person
  ORDNANCESURVEY = "http://data.ordnancesurvey.co.uk/doc/postcodeunit/"
	
	def initialize
	end
	
	def sparqly
	  @sparql = <<-EOL
    PREFIX hub: <http://data.archiveshub.ac.uk/def/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>

    SELECT ?given_name ?family_name ?occupation ?dob ?dod ?person ?dbpedia
    WHERE {
      ?person a <http://xmlns.com/foaf/0.1/Person>;
              foaf:givenName ?given_name;
         foaf:familyName ?family_name;
         hub:dateBirth ?dob;
         hub:dateDeath ?dod;
         hub:epithet ?occupation;
         <http://www.w3.org/2000/01/rdf-schema#label> ?label.
         OPTIONAL {
           ?person owl:sameAs ?dbpedia;
         }
    }
		EOL
		
		@glimmer = Glimmer.new
	end
  
  def lookup
    @results = @glimmer.sparql_select(@sparql,false)
		harry = Array.new
		parsed = JSON.parse(@results.content)
		parsed['results'].each do |result|
			result[1].each do |stuff|
				hashy = Hash.new
		    parsed['head']['vars'].each do |values|
				  case type = values
		        when 's'
							hashy.store("s",stuff[values]['value'])
		        when 'given_name'
		          hashy.store("given_name",stuff[values]['value'])
						when 'family_name'
		          hashy.store("family_name",stuff[values]['value'])
						when 'occupation'
		          hashy.store("occupation",stuff[values]['value'])
						when 'dob'
		          hashy.store("dob",stuff[values]['value'])
						when 'dod'
		          hashy.store("dod",stuff[values]['value'])
		        when 'person'
		          hashy.store("person",stuff[values]['value'])
		        when 'dbpedia' # is either dbpedia or viaf or other
		          begin
		            hashy.store("dbpedia",stuff[values]['value'])
	            rescue
              end
						when 'p','concept','o'
							# do nothing
						else
		          raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
		      end
				end
				harry << hashy
		  end
		end
    harry
  end
  
  def load_graph(uri)
    begin
      @graph = RDF::Graph.load(uri)
      @graph.load!
      @graph
    rescue Exception => e
    end
  end
  
  def query_graph
    harry = Array.new
    result = ""
    @graph.query([nil, "http://www.w3.org/2002/07/owl#sameAs", nil]) do |statement|
      result = triples(statement)
    end
    result
  end
  
  def triples(triple)
    result = ""
    case
      when triple.object.to_s.include?("http://dbpedia.org/resource/")
        result = triple.object.to_s
      when triple.object.to_s.include?("http://viaf.org/viaf/")
    end
    result
  end
end