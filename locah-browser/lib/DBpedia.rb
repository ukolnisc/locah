require 'rubygems'
require 'json'
require 'linkeddata'
require 'rdf'
require 'rdf/ntriples'

class DBpedia
  def initialize
  end
  
  def load_graph(uri)
    begin
      @graph = RDF::Graph.load(uri)
      @graph.load!
    rescue Exception => e
    end
    @graph
  end
  
  def query_graph
    quieries = [{"thumbnail" => "http://dbpedia.org/ontology/thumbnail"},
                {"born" => "http://dbpedia.org/property/dateOfBirth"},
                {"died" => "http://dbpedia.org/property/dateOfDeath"},
                {"nationality" => "http://dbpedia.org/property/nationality"},
                {"education" => "http://dbpedia.org/property/education"},
                {"ocupation" => "http://dbpedia.org/property/occupation"},
                {"spouse" => "http://dbpedia.org/property/spouse"},
                {"children" => "http://dbpedia.org/property/children"},
                {"parents" => "http://dbpedia.org/property/parents"}]# ,
                # {"signature" => "http://dbpedia.org/property/signature"}]
    harry = Array.new
    quieries.each do |query|
      la_query = dbpedia_query(query.values).to_s
      harry << {query.keys.first.to_s => la_query}
    end
    harry
  end
  
  def dbpedia_query(query)
    result = ""
    @graph.query([nil, query.first, nil]) do |statement|
      result = statement.object
    end
    result
  end
end