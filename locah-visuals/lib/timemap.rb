require 'rubygems'
require 'rdf'
require 'linkeddata'

require "./glimmer"

class Timemap
	ORDNANCESURVEY = "http://data.ordnancesurvey.co.uk/doc/postcodeunit/"
	
	def initializes
	end
	
	def sparql(term)
		@query = <<-EOL
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX hub: <http://data.archiveshub.ac.uk/def/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

    SELECT DISTINCT ?person ?given_name ?family_name ?origination ?associated ?occupation ?concept ?archresource ?reponame ?postcode ?extent ?datestart ?dateend ?date ?see_also ?same_as ?life_dates
    WHERE {
      ?person a foaf:Person ;
        foaf:givenName ?given_name;
        foaf:familyName ?family_name;
           hub:epithet ?occupation.
    FILTER (REGEX(STR(?occupation), "#{term}", "i")) 
    ?concept foaf:focus ?person.
    ?s ?p ?concept.
    ?s <http://data.archiveshub.ac.uk/def/accessProvidedBy> ?repo.
    ?repo <http://www.w3.org/2000/01/rdf-schema#label> ?reponame;
      <http://data.archiveshub.ac.uk/def/administers> ?place.
    ?place <http://www.geonames.org/ontology#postalCode> ?postcode.
    ?archresource hub:associatedWith ?concept;
    hub:extent ?extent.
    OPTIONAL {
      ?archresource hub:dateCreatedAccumulated ?date.
    }
    OPTIONAL {
       ?archresource hub:dateCreatedAccumulatedStart ?datestart.
    }
    OPTIONAL {
       ?archresource hub:dateCreatedAccumulatedEnd ?dateend.
    }
    OPTIONAL {
       ?archresource rdfs:seeAlso ?see_also.
    }
    OPTIONAL {
      ?archresource hub:origination ?origination.
    }
    OPTIONAL {
      ?archresource hub:associatedWith ?associated.
    }
    OPTIONAL {
       ?person owl:sameAs ?same_as.
    }
    OPTIONAL {
       ?person hub:dates ?life_dates.
    }
    }
		EOL
	end
	
	def lookup
	  puts "Looking up"
		@glimmer = Glimmer.new

		@results = @glimmer.sparql_select(@query,false)
		duplicate = String.new
		@harry = Array.new
		parsed = JSON.parse(@results.content)
		parsed['results'].each do |result|
			result[1].each do |stuff|
			  if !duplicate.eql?(stuff["archresource"]["value"].to_s)
  				hashy = Hash.new
  		    parsed['head']['vars'].each do |values|
  				  case type = values
				      when 'person' 
				        hashy.store("person",stuff[values]['value'])
				      when 'concept'
				        hashy.store("concept",stuff[values]['value'])
  		        when 's'
  							hashy.store("s",stuff[values]['value'])
  		        when 'given_name'
  		          hashy.store("given_name",stuff[values]['value'])
  						when 'family_name'
  		          hashy.store("family_name",stuff[values]['value'])
  		        when 'origination'
                begin
                  hashy.store("origination",stuff[values]['value'])
                rescue Exception => e
                end
              when 'associated' 
                begin
                  hashy.store("associated",stuff[values]['value'])
                rescue Exception => e
                end
  						when 'occupation'
  		          hashy.store("occupation",stuff[values]['value'])
  						when 'archresource'
  		          hashy.store("archresource",stuff[values]['value'])
  		        when 'reponame'
                hashy.store("reponame",stuff[values]['value'])
  		        when 'extent'
  	          	hashy.store("extent",stuff[values]['value'])
              when 'postcode'
                hashy.store("postcode",stuff[values]['value'])
              # when 'prefLabel'
              #   hashy.store("prefLabel",stuff[values]['value'])
              when 'date'
                begin
                  hashy.store("date",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end
  						when 'datestart'
  						  begin
  						    hashy.store("datestart",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end
  						when 'dateend'
  						  begin
  						    hashy.store("dateend",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end 
  						when 'same_as'
  						  begin
  						    hashy.store("same_as",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end
  						when 'see_also'
  						  begin
  						    hashy.store("see_also",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end
  						when 'life_dates'
  						  begin
  						    hashy.store("life_dates",stuff[values]['value'])
  						  rescue NoMethodError => e
  						  end
  						when 'p','o'
  							# do nothing
  						else
  		          raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
  		      end
  				  duplicate = stuff["concept"]["value"].to_s
			    end
          @harry << hashy
				end
		  end
		end
		@harry
	end
	
  # return in person is an originator or associate
  def filter_by_person
    @harry.each do |value|
      puts value["given_name"]
      puts value["family_name"]
      puts value['occupation']
      puts "****************************"
    end
  end
  
  def filter_by_term(terms=[])
    @harry.each do |value|
      if value['prefLabel'].include?("Political science") # value['occupation'].include?("")
        puts value["given_name"]
        puts value["family_name"]
        puts value['occupation']
        puts value['prefLabel']
        puts "****************************"
      end
    end
  end
  
  def results_size
    @harry.size
  end
  
  def test_results
    @harry.each do |value|
      puts value.inspect
    end
  end

  def remove_duplicates
    # get unique list of all pepople
    puts "removing duplicates"
    list_people = []
    return_array = []
    @harry.each {|value| list_people << "#{value["given_name"]} #{value["family_name"]}"}
    
    list_people.uniq!
    list_people.each do |person|
      # temp = all the entries for person
      temp = @harry.select{|key| "#{key["given_name"]} #{key["family_name"]}".eql?(person) }
      return_array << tidy_array(temp)
    end
    return_array
  end

  def tidy_array(array)
    new_hash = array.first

    new_array = []
    array.each do |item|
      compare = item.diff(new_hash)
      new_array << compare unless compare.empty?
    end

    new_array << new_hash

    new_array = new_array.reduce({}) {|h,pairs| pairs.each {|k,v| (h[k] ||= []) << v}; h}

    return_array = []
    new_array.each do |item|
      return_array << {item[0] => item[1].uniq}
    end
    # returns a single hash
    return_array.inject(:merge)
  end
  
  def generate_data(array, file_name)
    result = ""
    array.each do |value|
      location = ordnancesurvey_lookup(value['postcode'].first)
      value['lat'] = location[:lat]
      value['long'] = location[:long]
      if value["origination"]
        result << jsonify(value,:origination)
      end
      if value["associated"]
        result << jsonify(value,:associated)
      end
      result << jsonify(value,nil)
    end
    result.chop!
    result.insert(0, '([').insert(-1, '])')
    File.open(file_name, "w") do |f|
      f.write(result)
    end
  end

  def jsonify(args,type)
    same_as = []
    see_also = []
    browse_hub_ld = []
    person_type = args["person"].join
    args["same_as"].each {|arg| same_as << "<a href=\\\"#{arg}\\\">#{arg}</a>" } unless args["same_as"].nil?
    args["see_also"].each {|arg| see_also << "<a href=\\\"#{arg}\\\">#{arg}</a>" } unless args["see_also"].nil?
    args["archresource"].each {|arg| browse_hub_ld << "<a href=\\\"#{arg}\\\">Browse Hub Linked Data:</a>" } unless args["archresource"].nil?
    
    origination = ""
    if !args["origination"].nil? and type == :origination
      origination = "<a href=\\\"#{args["origination"].join}\\\">Origination</a><br />"
      person_type = args["concept"].join
    end
    
    associated = []
    if !args["associated"].nil? and type == :associated
      args["associated"].each {|arg| associated << "<a href=\\\"#{arg}\\\">Associated</a>" } unless args["associated"].nil?
      # associated = "<a href=\\\"#{args["associated"].join}\\\">Associated</a><br />"
      person_type = args["concept"].join
    end
    
    # puts associated
    given_name = args["given_name"].join || ""
    family_name = args["family_name"].join || ""
    life_dates = args["life_dates"].join || ""
        
    person = "<a href=\\\"#{person_type}\\\">Person</a><br />" unless person_type.nil?
        
    datestart = args["datestart"].join unless args["datestart"].nil?
    dateend = args["dateend"].join unless args["dateend"].nil?
    lat = args["lat"] || ""
    long = args["long"] || ""
    occupation = args["occupation"].join || ""
    extent = args["extent"].join || ""
    reponame = args["reponame"].join('; ') || ""
    theme = "green"
    tags = ""
    
    if !args["origination"].nil? and type.eql?(:origination)
      theme = "blue"
      tags = "originator"
    end
    
    if !args["associated"].nil? and type.eql?(:associated)
      theme = "red"
      tags = "associated"
    end
    
    "{\"title\":\"#{given_name} #{family_name} - #{life_dates}\","+    "\"start\":\"#{datestart}\",\"end\":\"#{dateend}\","+
    "\"point\":{\"lat\":\"#{lat}\",\"lon\":\"#{long}\"},"+
    "\"options\":{\"theme\": \"#{theme}\",\"description\":"+
    "\"#{person}"+
    "Occupation: #{occupation}<br />Size of archive: #{extent}<br />"+
    "Dates of archive: #{datestart} - #{dateend}<br />"+
    "#{reponame}<br />"+
    "#{origination}"+
    "#{associated.join(', ')}<br />"+
    "Same as: #{same_as.join(', ')}<br />"+
    "See Also: #{see_also.join(', ')}<br />"+
    "#{browse_hub_ld.join(', ')}\","+
    "\"tags\": [\"#{tags}\"]}},"
  end
  
  def ordnancesurvey_lookup(slug)
    uri = URI(ORDNANCESURVEY+slug.gsub(/\s+/, "")+".rdf")
    result = Hash.new
    lat = ""
    long = ""
    begin
      graph = RDF::Graph.load(uri)
      graph.load!
      
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#lat", nil]) do |statement|
        lat = statement.object
      end
      
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#long", nil]) do |statement|
      long = statement.object
    end
      result = {:lat => lat, :long => long}
    rescue Exception => e
      # if 404 could be non UK post-code
    end
  end
end

class Hash
  # Returns a hash that represents the difference between two hashes.
  #
  # Examples:
  #
  #   {1 => 2}.diff(1 => 2)         # => {}
  #   {1 => 2}.diff(1 => 3)         # => {1 => 2}
  #   {}.diff(1 => 2)               # => {1 => 2}
  #   {1 => 2, 3 => 4}.diff(1 => 2) # => {3 => 4}
  def diff(h2)
    dup.delete_if { |k, v| h2[k].downcase.to_s.eql?(v.downcase.to_s) }.merge!(h2.dup.delete_if { |k, v| has_key?(k) })
  end
end

tm = Timemap.new
tm.sparql("Travel|Explor")
tm.lookup
array = tm.remove_duplicates
tm.generate_data(array,"travel-and-explore")

tm.sparql("Poli")
tm.lookup
array = tm.remove_duplicates
tm.generate_data(array,"politics")

tm.sparql("Sci")
tm.lookup
array = tm.remove_duplicates
tm.generate_data(array,"science")


#Travel|Explor
#Poli
#Sci