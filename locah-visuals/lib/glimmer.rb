require 'rubygems'
require 'json'
require 'pho'
require 'rdf'
require 'yaml'

class Glimmer
	
	# need to change to load from conf or accept params
	def initialize
		# TODO: need to use relitive file paths
		conf = File.join(File.dirname("../../"), 'conf')
		@yammy = YAML.load_file("#{conf}/config.yaml")
		@storename = @yammy["config"]["storename"]
		@user = @yammy["config"]["user"]
		@pass = @yammy["config"]["pass"]
		
		@store = Pho::Store.new("#{@storename}", @user, @pass)

		@sparql_file = YAML.load_file("#{conf}/sparql.yaml")
	end
	
	def sparql_select(query="",convert=true)
		if convert
			convert_results(@store.sparql_select(query, "application/json"))
		else
			@store.sparql_select(query, "application/json")
		end
	end
	
	def select_query(query="")
		case query
			when "person"
				@sparql_file["query"]["person"]
			when "extent"
				@sparql_file["query"]["extent"]
			when "epithet"
				@sparql_file["query"]["epithet"]
			when "property"
				@sparql_file["query"]["property"]
			when "type"
				@sparql_file["query"]["type"]
		end
	end
	
	def convert_results(data="")
		results = JSON.parse(data.content)

		harry = Array.new

		results['results'].each do |result|
			result[1].each do |stuff|
				hashy = Hash.new
		 		results['head']['vars'].each do |values|
					case type = values # type is unnessary here.
		        when 's'
							hashy.store("s",stuff[values]['value'])
		        when 'p'
		          hashy.store("p",stuff[values]['value'])
		        when 'o'
							hashy.store("o",stuff[values]['value'])
		        else
		          raise RDF::ReaderError, "expected 'type' to be 's', 'p', or 'o', but got #{type.inspect}"
		      end
				end
				harry << hashy
			end
		end
		harry
	end
end