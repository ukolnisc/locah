# coding: utf-8
require 'rubygems'

# If you're using bundler, you will need to add this
require 'bundler/setup'

require 'sinatra/base'
require 'sinatra_more/markup_plugin'
require 'sinatra_more/routing_plugin'
require "sinatra/jsonp"
require 'json'
require 'net/http'
require 'uri'

class LocahVisualsApp < Sinatra::Base
  register SinatraMore::MarkupPlugin
	register SinatraMore::RoutingPlugin
	helpers Sinatra::Jsonp
	
	dp = /^[0-9]{4}-?[0-9]{2}-?[0-9]{2}$/
	
	helpers do
	  include Rack::Utils
	  alias_method :h, :escape_html
	end
	
  def replace(letter)
    if letter.eql?(" a")
      " a"
    else
      letter.upcase
    end
  end

  def capitalize_each(string)
    string.gsub(/(\A|\s)\w/){|letter| replace(letter)}
  end
	
	configure do |app|
    set :views, File.dirname(__FILE__) + "/../views"
    set :public, File.dirname(__FILE__) + "/../public"
  end
	
	get '/' do
		erb :index
	end
		
	get '/timemap/subject/:slug' do
	  @slug = params[:slug].gsub(/-/, ' ')
		erb :timemap
	end
		
	get '/timemap/data' do
    @slug = params['slug']
    puts @slug
	  
    case @slug.to_s
    when  "/timemap/subject/travel-and-exploration"
      data = travel
    when "/timemap/subject/science"
	    data = science
	  when "/timemap/subject/politics"
	    data = politics
	  end
    
   	if params["callback"] and params["from"] and params["to"]
			fromdate = params['from']
			todate = params['to']
			callback = params['callback']
		else
			return {}
		end
		response.headers['Content-Type'] = "application/json"
		resp = "#{callback}#{data}"
		return resp
  end
	
	get '/subject/:slug' do
		params[:slug]
	end	

  def science
    data = File.new("data/science", "r")
    data.read
  end
 
  def travel
    data = File.new("data/travel-and-explore", "r")
    data.read
  end
     
  def politics
    data = File.new("data/politics", "r")
    data.read
  end
end