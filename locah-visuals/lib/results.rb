require 'rubygems'
require 'rdf'
require 'linkeddata'
require 'json'
require 'yajl'

require "glimmer"

class Results
	ORDNANCESURVEY = "http://data.ordnancesurvey.co.uk/doc/postcodeunit/"
  
  attr_accessor :sparql_agent
  attr_accessor :agent_sparql
  attr_accessor :connections_sparql
  attr_accessor :origination_assosiated
  
	def initialize
	  @sparql_agent = <<-EOL
      PREFIX hub: <http://data.archiveshub.ac.uk/def/>
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

      SELECT ?agent ?name ?occupation ?concept ?repo ?reponame ?place ?postcode ?extent
      WHERE {
        ?agent a foaf:Agent;
          foaf:name ?name;
          hub:epithet ?occupation.
        ?concept foaf:focus ?agent.
        ?s ?p ?concept.
        ?s <http://data.archiveshub.ac.uk/def/accessProvidedBy> ?repo.
        ?repo <http://www.w3.org/2000/01/rdf-schema#label> ?reponame;
          <http://data.archiveshub.ac.uk/def/administers> ?place.
        ?place <http://www.geonames.org/ontology#postalCode> ?postcode.
        ?archresource hub:associatedWith ?concept;
          hub:extent ?extent;
        OPTIONAL {
          ?archresource hub:dateCreatedAccumulated ?date.
        }
        OPTIONAL {
          ?archresource hub:dateCreatedAccumulatedStart ?datestart.
        }
        OPTIONAL {
          ?archresource hub:dateCreatedAccumulatedEnd ?dateend.
        }
      }
  	EOL

  	@agent_sparql = <<-EOL
    PREFIX hub: <http://data.archiveshub.ac.uk/def/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

    SELECT ?agent ?name ?types ?origination ?occupation
    WHERE {
      ?agent a foaf:Agent;
        foaf:name ?name.
      ?agent a ?types.
      OPTIONAL {
        ?agent hub:epithet ?occupation;
      }
      OPTIONAL {
        ?agent hub:isOriginationOf ?origination.
      }
    }
  	EOL
  	
  	@connections_sparql = <<-EOL
    PREFIX hub: <http://data.archiveshub.ac.uk/def/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

    SELECT ?agent ?connection
    WHERE {
      ?agent a foaf:Agent.
      OPTIONAL {
        ?s ?connection ?agent.
      }
    }
  	EOL
  	
  	@origination_assosiated = <<-EOL
  	PREFIX hub: <http://data.archiveshub.ac.uk/def/>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

    SELECT ?agent ?name ?types ?origination ?associated ?occupation
    WHERE {
      ?agent a foaf:Agent;
        foaf:name ?name.
      ?agent a ?types.
      OPTIONAL {
        ?agent hub:epithet ?occupation;
      }
      OPTIONAL {
        ?agent hub:isOriginationOf ?origination.
      }
      OPTIONAL {
        ?archresource hub:associatedWith ?associated.
      }
    }
    EOL
	end
	 
	def sparql(query)
	  @glimmer = Glimmer.new
#?agent ?name ?occupation ?concept ?repo ?reponame ?place ?postcode ?extent
		@results = @glimmer.sparql_select(query,false)
    # puts @results.content
    @harry = Array.new
    parsed = JSON.parse(@results.content)
    # puts parsed.inspect
    parsed['results'].each do |result|
        result[1].each do |stuff|
          hashy = Hash.new
          parsed['head']['vars'].each do |values|
            case values
              when 'agent'
                hashy.store("agent",stuff[values]['value'])
              when 'name'
                hashy.store("name",stuff[values]['value'])
              when 'types'
                hashy.store("types",stuff[values]['value'])
              when 'origination'
                begin
                  hashy.store("origination",stuff[values]['value'])
                rescue Exception => e
                end
              when 'associated'
                begin
                  hashy.store("associated",stuff[values]['value'])
                rescue Exception => e
                end
              when 'occupation'
                begin
                  hashy.store("occupation",stuff[values]['value'])
                rescue Exception => e
                end
              when 'concept'
                hashy.store("concept",stuff[values]['value'])
              when 'repo'
                hashy.store("repo",stuff[values]['value'])
              when 'reponame'
                hashy.store("reponame",stuff[values]['value'])
              when 'place'
                hashy.store("place",stuff[values]['value'])
              when 'repo'
                hashy.store("repo",stuff[values]['value'])
              when 'postcode'
                hashy.store("postcode",stuff[values]['value'])
              when 'extent'
                hashy.store("extent",stuff[values]['value'])
              when 'connection' 
                hashy.store("connection",stuff[values]['value'])
              when 'p','person','concept','o'
                # do nothing
              else
                raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
            end
          end
          @harry << hashy
        end
      end
  end

  def sparql_and_save(query)
    @glimmer = Glimmer.new
  	@results = @glimmer.sparql_select(query,false)
    # data = JSON.parse(@results.content)
    data = Yajl::Encoder.encode(@results.content)
    to_file("origination_assosiated",data)
  end
  
  def save(file,_data)
    data = Yajl::Encoder.encode(_data)
    to_file(file,data)
  end
  
  def sparql_and_open(file)
    @harry = Array.new
    json = File.new("#{file}.json", 'r')
    hash = Yajl::Parser.parse(json)
    parsed = JSON.parse(hash)
    parsed['results'].each do |result|
      result[1].each do |stuff|
        hashy = Hash.new
        parsed['head']['vars'].each do |values|
          case values
            when 'agent'
              hashy.store("agent",stuff[values]['value'])
            when 'name'
              hashy.store("name",stuff[values]['value'])
            when 'types'
              hashy.store("types",stuff[values]['value'])
            when 'origination'
              begin
                hashy.store("origination",stuff[values]['value'])
              rescue Exception => e
              end
            when 'occupation'
              begin
                hashy.store("occupation",stuff[values]['value'])
              rescue Exception => e
              end
            when 'concept'
              hashy.store("concept",stuff[values]['value'])
            when 'repo'
              hashy.store("repo",stuff[values]['value'])
            when 'reponame'
              hashy.store("reponame",stuff[values]['value'])
            when 'place'
              hashy.store("place",stuff[values]['value'])
            when 'repo'
              hashy.store("repo",stuff[values]['value'])
            when 'postcode'
              hashy.store("postcode",stuff[values]['value'])
            when 'extent'
              hashy.store("extent",stuff[values]['value'])
            when 'connection' 
              hashy.store("connection",stuff[values]['value'])
            when 'p','person','concept','o'
              # do nothing
            else
              raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
          end
        end
        @harry << hashy
      end
    end
    @harry
  end
  
  def to_file(file,data)
    # ObjectStash.store data, "./#{file}.stash"
    File.open("#{file}.json","w") do |f|
      f.write(data)
    end
  end
  
  def from_file(file)
    Yajl::Parser.parse(File.new("#{file}.json", 'r'))
  end
  
  def remove_organisations
    puts "Removing organisations"
    @array_builder = Array.new
    puts "BEFORE: #{@harry.size}"
    @harry.each do |value|
      if value["types"].to_s.eql?("http://purl.org/dc/terms/Agent")
        if !value["origination"].to_s.empty?
          @array_builder << value
        end
      end
      if value["types"].to_s.eql?("http://xmlns.com/foaf/0.1/Person")
        @array_builder << value
      end
    end
    puts "AFTER: #{@array_builder.size}"
    @harry = @array_builder
  end
  
  def get_originators
    @array_builder = Array.new
    @harry.each do |value|
      if value["types"].to_s.eql?("http://purl.org/dc/terms/Agent")
        if !value["origination"].to_s.empty?
          @array_builder << value
        end
      end
    @array_builder
  end
  
  def remove_duplicates(array)
    @array_builder = Array.new
    # puts "BEFORE: #{array.size}"
    
    current_person = Array.new
    
    array.each do |value|
      # puts current_person.empty?
      if !current_person.empty?
        # puts "CURRENT_PERSON-NAME #{current_person["name"]}"
        # puts "VALUE-NAME #{value["name"]}"
        if current_person["name"].to_s.include?(value["name"].to_s) || value["name"].to_s.include?(current_person["name"])
          # puts "CURRENT_PERSON-NAME2 #{current_person["name"]}"
          # puts "VALUE-NAME2 #{value["name"]}"
          if !current_person["origination"].to_s.empty?
            # puts "CURRENT_PERSON-ORIGINATION #{current_person["origination"]}"
            @array_builder << current_person
          end
          if value["types"].eql?("http://xmlns.com/foaf/0.1/Person")
            # puts "VALUE-TYPE #{value["types"]}"
            @array_builder << value
          end
        end
      end
      current_person = value
    end
    
    # puts "AFTER: #{@array_builder.size}"
    @array_builder
  end
  
  def get_connections(array)
    puts "Finding only people"
    # http://purl.org/vocab/bio/0.1/principal
    @array_builder = Array.new
    puts "BEFORE: #{array.size}"
    array.each do |value|
      # puts value.inspect
      if value["connection"].to_s.eql?("http://purl.org/vocab/bio/0.1/principal")
        # puts value.inspect
        @array_builder << value
        # puts "************************"
      end
    end
    puts "AFTER: #{@array_builder.size}"
    @array_builder
  end
  
  def display_data
    @harry.each do |value|
      puts "Value => #{value['name']}"
      puts "Origination => #{value['origination']}" unless value["origination"].eql?(nil)
      puts "Associated => #{value['associated']}" unless value["origination"].eql?(nil)
      puts "Types => #{value['types']}"
      puts "Agent => #{value['agent']}"
    end
  end
  
  def get_current_array
    @harry
  end

  def generate_data(array,file_name)
    result = ""
    array.each do |value|
      # puts value.inspect
        location = ordnancesurvey_lookup(value['postcode'])
      # puts location
        value['lat'] = location[:lat]
        value['long'] = location[:long]
      # puts value.inspect
        result << jsonify(value)
    end
    result.chop!
    result.insert(0, '([').insert(-1, '])')
    File.open(file_name, "w") do |f|
      f.write(result)
    end
  end

  def jsonify(args)
    "{\"title\":\"#{args["given_name"]} #{args["family_name"]} - #{args["life_dates"]}\","+
    "\"start\":\"#{args["datestart"]}\",\"end\":\"#{args["dateend"]}\","+
    "\"point\":{\"lat\":\"#{args["lat"]}\",\"lon\":\"#{args["long"]}\"},"+
    "\"options\":{\"description\":"+
    # "\"Name: #{args["given_name"]} #{args["family_name"]}<br />"+
    "\"Occupation: #{args["occupation"]}<br />Size of archive:#{args["extent"]}<br />"+
    "Dates of archive: #{args["datestart"]} - #{args["dateend"]}<br />"+
    "#{args["reponame"]}<br />"+
    "Same as: <a href=\\\"#{args["same_as"]}\\\">#{args["same_as"]}</a><br />"+
    "See Also: <a href=\\\"#{args["see_also"]}\\\">#{args["see_also"]}</a><br />"+
    "Browse Hub Linked Data: <a href=\\\"#{args["archresource"]}\\\">#{args["archresource"]}</a>\"}},"
  end
	
	def ordnancesurvey_lookup(slug)
    @uri = URI(ORDNANCESURVEY+slug.gsub(/\s+/, "")+".rdf")
    @lat = ""
    @long = ""
    begin
      graph = RDF::Graph.load(@uri)
      graph.load!
    
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#lat", nil]) do |statement|
        @lat = statement.object
      end
    
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#long", nil]) do |statement|
        @long = statement.object
      end
    rescue Exception => e
      # if 404 could be non UK post-code
    end
  end
end

r = Results.new
r.sparql(r.origination_assosiated)
r.generate_data(r.get_originators,"originators")
# r.display_data