require 'rubygems'
require 'csv'
require 'json'
require 'linkeddata'
require 'rdf'
require 'rdf/ntriples'

require File.expand_path("../../locah-web/lib/glimmer.rb", __FILE__)

class LocahGenerateTriples

  # Subject = Hub URI (Column B)
  # Predicate = owl:sameAs or umbel:isLike (based on Column E)
  # Object = Viaf URI (Column D plus /#foaf:Person)
  # 
  # And
  # 
  # Subject = Hub URI (Column B)
  # Predicate = owl:sameAs or umbel:isLike (based on Column E)
  # Object = DBpedia URI (EITHER from Column F (just one case) OR from looking up VIAF URI and grabbing DBpedia URI from VIAF record)
  # {A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6}
  
  # Column C = URL for Hub data
  # Column D = URL for VIAF, changed where required
  # Column E = status
  
  NAME = 0
  DATA_DOT_ARCHIVE_HUB_URI = 1
  MATCH = 4
  ARCHIVE_HUB_URI = 2
  VIAF_URI = 3

  EXACT_MATCH = "http://www.w3.org/2002/07/owl#sameAs"
  LIKELY_MATCH = "http://umbel.org/umbel#isLike"
  
  def initialize
  end
  
  def load_csv
    data = CSV.open("locah-namematch-2June2011.csv", 'r')
    data.shift
    triples = Array.new
    data.each do |row|
      dbpedia = add_dbpedia(row[VIAF_URI])
      if !dbpedia.eql?("")
        triples <<  RDF::Statement.new(RDF::URI.new(row[DATA_DOT_ARCHIVE_HUB_URI]),
                                       RDF::URI.new(EXACT_MATCH),
                                       RDF::URI.new(dbpedia))
      end
      case row[MATCH]
        when "sameAs"
          triples << RDF::Statement.new(RDF::URI.new(row[DATA_DOT_ARCHIVE_HUB_URI]),
                                        RDF::URI.new(EXACT_MATCH),
                                        RDF::URI.new(row[VIAF_URI]+"#foaf:Person"))
        when "isLike"
          triples << RDF::Statement.new(RDF::URI.new(row[DATA_DOT_ARCHIVE_HUB_URI]),
                                        RDF::URI.new(LIKELY_MATCH),
                                        RDF::URI.new(row[VIAF_URI]+"#foaf:Person"))
        when "/"
        else
      end
    end
    generate_triple(triples)
  end
	
  def generate_triple(data)
    # puts "<#{subject}>#{predicate}<#{object}>"
    RDF::Writer.open("viaf-sameas-6-June-2011.nt") do |writer|
      data.each do |statement|
        writer << statement
      end
    end
  end
  
  private
  
    def add_dbpedia(uri="")
      # <owl:sameAs rdf:resource="http://dbpedia.org/resource/Ernest_Shackleton"/>
      result = ""
      if !uri.to_s.end_with?("/")
        uri = uri.to_s << "/"
      end
      puts uri
      viaf_url = "#{uri}rdf.xml"
      
      begin
        graph = RDF::Graph.load(viaf_url)
        graph.load!
        graph.query([nil, "http://www.w3.org/2002/07/owl#sameAs", nil]) do |statement|
          result = statement.object if statement.object.to_s.include?("http://dbpedia.org/")
        end
      rescue Exception => e
      end
      result
    end
end

generate = LocahGenerateTriples.new
generate.load_csv