require 'rubygems'
require 'sinatra/base'
require 'sinatra_more/markup_plugin'
require 'sinatra_more/routing_plugin'
require 'rdf'
require 'rdf/json'

require 'glimmer'

class LocahApp < Sinatra::Base
	register SinatraMore::MarkupPlugin
	register SinatraMore::RoutingPlugin

	map(:root).to("/")
	map(:uri).to("/uri/:query")
	map(:result).to("/results/:query")
	map(:results).to("/results")
	map(:enrich).to("/enrich")
	map(:enrich).to("/enrich/:query")
	
	helpers do
	  include Rack::Utils
	  alias_method :h, :escape_html
	end
	
	configure do |app|
    set :views, File.dirname(__FILE__) + "/../views"
    set :public, File.dirname(__FILE__) + "/../public"
  end
	
	get '/' do
		erb :index
	end
	
	get '/uri/*' do
		@glimmer = Glimmer.new

		SPARQL = <<-EOL
		SELECT *
		WHERE {
		  <http://data.archiveshub.ac.uk/#{params[:splat]}> ?p ?o.
		}
		EOL

		@results = @glimmer.sparql_select(SPARQL)

		erb :results
	end
	
	get '/results' do
		erb :results
	end
	
	get '/results/:query' do
		@glimmer = Glimmer.new
		query = @glimmer.select_query(params[:query])
		
		@results = @glimmer.sparql_select(query)		
		erb :results
	end
	
	get '/enrich' do
		erb :enrich
	end
	
	get '/enrich/:query' do
		erb :enrich
	end

end