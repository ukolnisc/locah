# Average Box size: Based on a sample of 15 averages from various repositories
# and taking into account 3 particularly deep boxes
# Box: 40 x 29 x 13.
# Average box by cubic metres: 0.02825
# Average number of boxes per shelf is more varied. Would have to use a rough estimate,
# maybe 10 boxes per shelf. 
# File: estimate 20% of a box. 5 files per box.

require 'rubygems'
require 'stemmer/porter'
require 'spellchecker'

require 'glimmer'
require 'rosso'

# TODO: What is this class? Enriching the Extent data, normalizing the extent data?
# Really need a big re-sort of classes and names, etc.
class Extent
	
	def initialize
		@glimmer = Glimmer.new
		@rosso = Rosso.new({:redis_key => "extent"})
		@query = @glimmer.select_query("extent")
	end
	
	def query_store
		@results = @glimmer.sparql_select(@query)
	end
	
	def cache_results
		@results.each do |result|
			@rosso.lpush(result['o'])
		end
	end
	
	def frequency
		text = @rosso.lrange.join(" ")
		words = text.split(/[^a-zA-Z]/)
		stop_words = Array.new
		words.each {|word| stop_words << word if word.length < 3}
		freqs = Hash.new(0)
		stop_words.uniq!
		stop_words.each {|word| words.delete(word)}
		words.uniq!
		words.each {|word| puts word.stem }
		text = "archiv"
		Spellchecker.aspell_path=('/usr/local/bin/aspell')
		Spellchecker.check(text, dictionary='en').each do |suggestion|
			suggestion.each do |value|
				puts value
			end
		end
			
		# words.each { |word| freqs[word] += 1 }
		# freqs = freqs.sort_by {|x,y| y }
		# freqs.reverse!
		# freqs.each {|word, freq| puts word+' '+freq.to_s}
	end
end

extent = Extent.new
# extent.query_store
# extent.cache_results
extent.frequency