require 'rubygems'
require 'json'

require 'glimmer'
require 'person'
require 'viaf'
require 'rosso'

# [{"o"=>"fl 1990", "p"=>"http://data.archiveshub.ac.uk/def/dates"}, 
# {"o"=>"cartoonist", "p"=>"http://data.archiveshub.ac.uk/def/epithet"},
# {"o"=>"http://data.archiveshub.ac.uk/id/floruit/ncarules/hollingsjuliefl1990cartoonist",
		# "p"=>"http://purl.org/vocab/bio/0.1/event"}, 
# {"o"=>"http://purl.org/dc/terms/Agent", "p"=>"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"},
# {"o"=>"http://xmlns.com/foaf/0.1/Agent", "p"=>"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"},
# {"o"=>"http://xmlns.com/foaf/0.1/Person", "p"=>"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"},
# {"o"=>"Hollings", "p"=>"http://xmlns.com/foaf/0.1/familyName"},
# {"o"=>"Julie", "p"=>"http://xmlns.com/foaf/0.1/givenName"}]

# TODO: Tidy up class, move sparql quiers out to glimmer.rb
class Enrich
	
	def initialize
		@glimmer = Glimmer.new
		@rosso = Rosso.new({:redis_key => "viaf"})
		sparql = <<-EOL
		 	PREFIX foaf: <http://xmlns.com/foaf/0.1/>

			SELECT *
			WHERE {
			  ?s ?p foaf:Person;
			  foaf:givenName ?given_name;
			  foaf:familyName ?family_name.
			}
			EOL
	
		@results = @glimmer.sparql_select(sparql,false)
		
		harry = Array.new
		parsed = JSON.parse(@results.content)
		parsed['results'].each do |result|
			result[1].each do |stuff|
				hashy = Hash.new
		    parsed['head']['vars'].each do |values|
				  case type = values
		        when 's'
							hashy.store("s",stuff[values]['value'])
		        when 'given_name'
		          hashy.store("given_name",stuff[values]['value'])
						when 'family_name'
		          hashy.store("family_name",stuff[values]['value'])
						when 'p'
							# do nothing
						else
		          raise RDF::ReaderError, "expected 'type' to be 's', 'given_name', or 'family_name', but got #{type.inspect}"
		      end
				end
				harry << hashy
		  end
		end
		
		viaf = Viaf.new
		harry.each do |value|
			# puts value['s']
			# puts value['given_name']
			# puts value['family_name']
			results = viaf.search("#{value['given_name']} #{value['family_name']}")
			results.each do |result|
				data = {"viaf_id" => result[:id], "name" => result[:names]}
				@rosso.lpush(data.to_json,value['s'])
				# puts @rosso.lrange(value['s'],0,-1)
			end
		end
		# @enriched_results = Array.new
		# @results.each do |result|
		#   @enriched_results.push(@glimmer.sparql_select(sparql,false))
		# end
  end
end

enrich = Enrich.new

# http://data.archiveshub.ac.uk/id/person/ncarules/hollingsjuliefl1990cartoonist
# Hollings, Julie
# 77855362
# ****************
# http://data.archiveshub.ac.uk/id/person/ncarules/eastwoodwendyfl1990cartoonist
# Schmid-Eastwood, Wendy
# 38895540
# Shmid-Eastwood, Wendy
# 26584587
# ****************
# http://data.archiveshub.ac.uk/id/person/ncarules/holdenwendyfl1990cartoonist
# Holden, Wendy, 1961-
# 75705469
# Holden, Wendy, 1965-
# 102127262
# Holden, Wendy
# 97909322
# Holden, Wendy
# 100117725
# Holden, Taylor, 1961-
# 4749711
# ****************