require 'rubygems'
require 'spira'

class Person

  include Spira::Resource

  property :family_name, :predicate => FOAF.familyName, :type => String
  property :given_name, :predicate => FOAF.givenName, :type => String

end

# person = RDF::URI("http://data.archiveshub.ac.uk/id/floruit/ncarules/hollingsjuliefl1990cartoonist").as(Person)
# person.family_name = "Julie"
# person.given_name = "Hollings"
# # person.save!
# 
# person.each_statement {|s| puts s}
# puts person.subject