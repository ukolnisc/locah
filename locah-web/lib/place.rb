require 'rubygems'
require 'json'
require 'net/http'
require 'uri'
require 'rdf'
require 'linkeddata'
require 'pho'

# require File.expand_path("../../../locah-web/lib/glimmer.rb", __FILE__)
require 'glimmer'

class Place
  
	attr_reader :lat
	attr_reader :long

  DBPEDIA = "http://dbpedia.org/resource/"
  ORDNANCESURVEY = "http://data.ordnancesurvey.co.uk/doc/postcodeunit/"
	
  def initialize
  end
  
	def ordnancesurvey_lookup(slug)
    uri = URI(ORDNANCESURVEY+slug.gsub(/\s+/, "")+".rdf")
    @lat = ""
    @long = ""
    begin
      graph = RDF::Graph.load(uri)
      graph.load!
    
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#lat", nil]) do |statement|
        @lat = statement.object
      end
    
      graph.query([nil, "http://www.w3.org/2003/01/geo/wgs84_pos#long", nil]) do |statement|
        @long = statement.object
      end
    rescue Exception => e
      # if 404 could be non UK post-code
    end
		puts "#{uri} lat: #{@lat}, long: #{@long}"  
  end
  
  def dbpedia_lookup(slug)
		uri = DBPEDIA+urlify(slug)
		sparql = <<-EOL
			SELECT * WHERE {
			<#{uri}> <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.
			<#{uri}> <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.
			}
		EOL
		@store = Pho::Store.new("http://dbpedia.org/sparql")
		
		@results = @store.sparql_select(sparql, "application/json")
    lat = ""
    long = ""
		parsed = JSON.parse(@results.content)
		parsed['results']['bindings'].each do |result|
			lat = result['lat']['value']
      long = result['long']['value']
		end
		puts "#{uri} lat: #{lat}, long: #{long}"
  end
  
  private
  
  # Convert place name from free text string into a format
  # that can be appended to a dpedia url
  def urlify(slug)
    slug = slug.split("(").first
    slug = slug.split(",").first
    slug = slug.strip.sub(/[^a-zA-Z ]/,"")
    slug.gsub(/[^a-z0-9\-_\+]+/i, "_")
  end
end

# Place.new("BD7 1DP")