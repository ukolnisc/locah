require 'rubygems'
require 'sru'
require 'nokogiri'

class Viaf

	def search(query,opts="")
		harry = Array.new
		client = SRU::Client.new("http://viaf.org/viaf/search", :parser=>"libxml")
		opts = {:maximumRecords=>5, :startRecord=>1, :recordSchema=>"VIAF"}
		results = client.search_retrieve("local.personalNames=#{query}", opts)

		results.each do |result|
			doc = Nokogiri::XML(result.to_s)
			names = Array.new
			hashy = Hash.new
			viaf_id = ""
			doc.xpath('//ns2:viafID', 'ns2' => "http://viaf.org/viaf/terms#").each do |node|
			  viaf_id = node.text
			end
			doc.xpath('//ns2:mainHeadings/ns2:data/ns2:text', 'ns2' => "http://viaf.org/viaf/terms#").each do |node|
				names.push(node.text)
			end
			hashy = {:id => viaf_id, :names => names}
			harry.push(hashy)
		end
		harry
	end
end
# 
# viaf = Viaf.new
# results = viaf.search("Ernest Shackleton")
# 
# results.each do |result|
# 	puts result[:id]
# 	puts result[:names]
# end