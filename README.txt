example-viaf-query.rb
 Pass in a name returns items related to that person from Viaf.
 
generate-triples/
  locah-generate-triples.rb
  Loads in a CSV file of archive hub uris, matches the names of the people from those uris against lookups on dbpedia and viaf. Generates a file of triples of Archive Hub uris, dbpedia uris and viaf uris and whether or not they are exact or likely matche, these triples can then be uploaded to the Talis store.
  
locah-browser/
  Simple work in progress demo app. Each page is corrisponds to a archive hub uri, will preform an on-the-fly lookup of Dbpedia, to see if there are any extra data there about the person concered in the uri, ie a thumbnail, date of birth/death, family, etc.
  
locah-init/
  Scripts to upload data to the Talis store.
  
locah-sameas/
  Simple work in progress demo app. Idea being that taking the same as data generated in locah-generate-triples.rb a user would be able to click on an item to decide if a uri in dbpedia was definitely the same person as the uri in archives hub.

locah-visuals/
  Geographic Timemap to display archive location of people mentioned within the archives hub data. http://ruby.ukoln.ac.uk/locah/timemap/subject/science for example.
  lib/results.rb
    Searches through Archives Hub data looking for person uris, and generates the necessary json files required for the application. The json files are stored in data/ under the name of the subject they are classified into. So far are politics, science, and travel and exploration.
  lib/locah-visuals-app.rb
    Is a Sinatra app that works with the simile timeline to display the json data. 

locah-web/
  Was built for testing purposes. Displays data from a archives hub uri, along with links between uris. Is basically a simplified version of http://data.archiveshub.ac.uk/doc/archivalresource/gb1086skinner as this wasn't available at the beginning of the Locah project.
  glimmer.rb
    Is used by most of the code in these folders to run the sparql query on the servers and convert the results into json.