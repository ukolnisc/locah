require 'rubygems'
require 'yaml'
require 'find'
require 'FileUtils'

require 'pho'

class LocahUtils
	
	def initialize
		conf = File.expand_path("../conf/config.yaml", __FILE__)
		@yammy = YAML.load_file(conf)
		@storename = @yammy["config"]["storename"]
		@user = @yammy["config"]["user"]
		@pass = @yammy["config"]["pass"]
		
		@store = Pho::Store.new(@storename, @user, @pass)
	end
	
	def read_from_store
		puts Pho::Jobs.read_from_store(@store)
	end

	def submit_reset
		begin
			resp = Pho::Jobs.submit_reset(@store)
			job = Pho::Jobs.wait_for_submitted(resp, @store) do | job, message, time |
			  puts "#{time} #{message}"
			end
		rescue Exception => e
		end
	end
end

locah_utils = LocahUtils.new
locah_utils.submit_reset