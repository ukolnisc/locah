require 'rubygems'
require 'yaml'
require 'find'
require 'FileUtils'

require 'pho'

class LocahUpload
	
	PHO	=	".pho".freeze
	
	def initialize
		conf = File.expand_path("../conf/config.yaml", __FILE__)
		@yammy = YAML.load_file(conf)
		@storename = @yammy["config"]["storename"]
		@user = @yammy["config"]["user"]
		@pass = @yammy["config"]["pass"]
		@local_data_store = @yammy["config"]["local_data_store"]
		
		@store = Pho::Store.new(@storename, @user, @pass)
	end
	
	def re_initialize_phos
		find_folders(:destroy)
	end
	
	def find_folders(create_or_destroy=:create)
		@harry = Array.new
		Find.find(@local_data_store) do |file| 
			if File.ftype(file) == "directory"
				# puts file
				create_or_destroy_phos(create_or_destroy,file)
				@harry.push(file) unless file.split("/").last.eql?(PHO)
			end
		end
	end
	
	def upload_to_store
		@harry.each do |folder|
			puts "Where am I:#{folder}"
			collection = Pho::FileManagement::RDFManager.new(@store, folder, ["xml,nt"])
			collection.store()
			puts collection.summary()
	  end
	end
	
	def upload_single_folder_to_store(file)
		collection = Pho::FileManagement::RDFManager.new(@store, file, ["xml,nt"])
		collection.store()
		puts collection.summary()
	end
	
	private
	
		def create_or_destroy_phos(create_or_destroy,file)
			case create_or_destroy
			   when :create then create_pho_folder!(file)
			   when :destroy then destroy_pho_folder!(file)
				 else :unknown
			end
		end
		
		def create_pho_folder!(file)
			location = "#{file}/#{PHO}"
			if File.directory?(location)
				return
			else
				Dir::mkdir(location) unless file.split("/").last.eql?(PHO)
			end
		end
		
		def destroy_pho_folder!(file)
			location = "#{file}/#{PHO}"
			if File.directory?(location)
				FileUtils.rm_rf location
			end
		end
end

locah_upload = LocahUpload.new
locah_upload.re_initialize_phos
locah_upload.find_folders
locah_upload.upload_to_store


